#############
Rental Rules
#############
.. note::

  These are the rental rules which are in place to maintain order and stability in the server.

.. _Upper Administration Team: https://forums.priorityzerogaming.com/support/
.. _Administration Team: https://forums.priorityzerogaming.com/support/
.. _Development Team: https://forums.priorityzerogaming.com/support/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _idea tracker: https://forums.priorityzerogaming.com/ideas/
.. _bug tracker: https://forums.priorityzerogaming.com/bugs/

General Provisions
===================
Renting is a luxury within Woodland County, it provides you a means to support the community and receive something in return for donating.

Renting can provide you in some cases an OOC place to relax and have downtime, give you an place to enhance your roleplay and much more.

.. important:: 

  In order to be fair: renting does **not** exempt you from any rules, and you will not be entitled to a refund if you are punished or banned.

No Refunds
----------
Rent paid is non-refundable. Unless otherwise decided by the Upper Administration Team.

Citizens Only
-------------
All rentals may be rented, upon availability, by a citizen or approved whitelisted player by paying the rental meter or rental mailbox.

Prim Limits
-----------
Renters are responsible for keeping track of the number of prims they have rezzed, and staying within their prim limits per their rental.

Habitually violating your rental allotment will result in arbitrary prim return to return you to your prim limit.

Additional Tenants
------------------
You must add any additional tenants to your rental otherwise their prims will be returned.

Arrears
-------
Rentals may be rented once the current tenant has gone into arrears, if you do not wish to lose your rental, be sure to keep your rent pre-paid in case of unforeseen circumstances.

Contact the Rental Team via the `Support Portal`_ if you need special arrangements.

Temp Rezzers & Lag Generating Items
-----------------------------------
Rentals may not contain temp rezzers, or objects that produce lag generating items. Such objects will be returned without warning. This includes but not limited to scripted items with high script times.

Please submit a request to the Rentals Team via the `Support Portal`_ if you require these.

Returning Items
---------------
The `Administration Team`_ may return any items left out, OOC, and especially those items exceeding a renters alloted prim amount.

Skyboxes
--------
Skyboxes must have prior approval, a minimum height for skyboxes is 1,000 meters and a maximum height is 1,500 meters. Any buils outside of this will be returned.

Agreement
---------
Your payment of any rental box is youracceptance and consent to current and future amendments of these rules, as well as agreement to abide by Linden Labs Terms of Service.

Residential Rentals
===================
Residential rentals are solely for the purpose of residing in.

The following restrictions are enforced:

#. Residential rentals may not be used for commercial use without permission from the `Upper Administration Team`_.
#. Residential lots are OOC, unless expressly made IC by the tenant by a sign out the front of the property. If you do not have a sign, you may request one via the `Support Portal` to the Rental Team.
#. Residential lots have additional IC rules attached to them to prevent abuse by others.

Commercial Rentals
==================
Commercial rentals are used for the intent of OOC profit gain and not roleplay.

The following restrictions are enforced:

#. Commercial rentals may not be used for residential rentals without permission from the `Upper Administration Team`_.
#. Commercial lots are always IC and cannot be made OOC.
#. Commercial lots may have roleplay occur within them.

Roleplay Rentals
================
Roleplay rentals are used for the intent of roleplay and are not for OOC profit gain.

The following restrictions are enforced:

#. Roleplay rentals are always IC. You can request permission from the `Upper Administration Team`_ for a special exemption.
#. If a roleplay rental is used for residential purposes, the `Administration Team`_ may change the pricing to be inline with the Residential rental pricing. However the Roleplay Rental rules will be the ones applied.

Prefabricated rentals
---------------------
Prefabricated rentals a type of roleplay rental which have been prefabricated and typically have higher traffic. They are for the sole intent of roleplay and are not for OOC profit gain, or residential.

The following restrictions are enforced:

#. Prefabricated rentals are generally ran by a designated owner or manager of the business.
#. Only a maximum of a week at a time may be placed on the rental.
#. Only minor modifications may be made to prefabricated rentals. Any major requests may be put forward to the `Development Team`_.
#. The `Upper Administration Team`_ reserve the right to evict you from the rental without refund if you are not active in holding the rental.
