#############
General Rules
#############
.. note::

  The Woodland County General Rules are all the rules that are related to players, accounts and not focused on in character actions.

.. _Upper Administration Team: https://forums.priorityzerogaming.com/support/
.. _Administration Team: https://forums.priorityzerogaming.com/support/
.. _Development Team: https://forums.priorityzerogaming.com/support/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _idea tracker: https://forums.priorityzerogaming.com/ideas/
.. _bug tracker: https://forums.priorityzerogaming.com/bugs/

18+ Only
========
All players must be 18+ only. We are an adult community, there are adult themes and adult language being used. Minor characters are moderated as per rules.

:Potential Punishment: Removal from the community

Toxicity and Bullying
=====================
Toxicity is defined as bringing unwanted attitudes into the community. There is a difference between having an opinion/voicing this opinion, and forcing your opinion on other people. Do not direct your anger/frustration/aggression towards another player. Use the appropriate forums for this.

:Potential Punishment: 7 day suspension

Derogatory Remarks (IC & OOC)
=============================
Negative/Derogatory remarks about; race, religion, gender, sexual preference, sexual orientation, gender identity, etc. is strictly prohibited. This includes in and out of RP.

:Potential Punishment: 7 day suspension

Do not contact developers directly
==================================
Please use the relevant forums to get help with your issues. Before attempting to message developers. Only message with permission if absolutely necessary.

:Potential Punishment: 2 day suspension

Scamming
========
Scamming of money, vehicles and properties is not allowed by any means both IC or OOC. In general this means performing an action which gives you legal possession of a property or vehicle through deceit.  

**Example of Scamming**
Promising to pay money after someone sells you a property or vechicle and not paying the money as promised.

**Example of Not Scamming**
Taking a property or vehicle through force or robbery, in this instance, possession was never taken legally through deceit.

:Potential Punishment: 7 day suspension

AFKing
======
Leaving your computer or in any manner tabbing out or going AFK for more than 15 minutes, should not be done in a roleplay area. Please be inside your private home, faction area or the welcome center.

If you are going to AFK in a roleplay area, you must have a tag on stating you are OOC or AFK.

:Potential Punishment: Ejected from sim

Exemption
----------
Factions inside their designated areas are excempt from the above rule. I.E. Police and fire may idle in their respective departments. Criminals may idle in their hideouts.

External Advertising
====================
Sending other players links or discussing other servers, communities or groups in the manner of advertising is strictly prohibited. We expect all players to keep their opinions about said matter to themselves, as we hope members from other corners of the gaming world would do the same for us. 

:Potential Punishment: 7 day suspension

Report Bugs
===========
You are not allowed to abuse bugs, bugs are considered as actions in which are not intended, such as duplication of items/money/vehicles.
You are required to report any bugs you find using our `bug tracker`_. 
Failure to report bugs and instead using them for your own benefit/gain will be met with a severe punishment (see below).

Exploiting
==========
Exploiting of a bug or glitch is strictly prohibited. This includes setting up macros to farm jobs. Purposely exploiting or macro’ing anything for personal gain will result in heavy consequences.

:Potential Punishment: 7 day suspension

Currencies
==========
There are two kinds of currencies. Out Of Character and In Character ones.

* In Character currencies include everything with money in Woodland County and similar used by your Character.
* Out Of Character currencies are L$ and any currency used by yourself, as a Player.

These currencies are not to be mixed. Exchanging in character currencies for out of character ones or vice-versa, is not allowed. They are to be kept separate.

:Potential Punishment: 30 day suspension

Impersonation
=============
Impersonating another player, by using a similar account name, creating a certain level of confusion is not allowed. Account names are considered personal and must remain so.

Portraying yourself as a staff member is strictly forbidden and may lead to harsh punishments. Woodland County chooses its staff members carefully and wishes to keep doing so.

:Potential Punishment: 3 day suspension

Inappropriate & Illegal Content
===============================
Do not post material or links to content which is illegal as per United States, Canadian, Australian and International law.

Do not post nudity, gore, or otherwise inappropriate content without spoilers, warnings, and a reason. For example, illegal roleplayers discussing cartels in real life and showing a video of a shootout would be acceptable, showing random gore for shock value is not.

:Potential Punishment: 3 day suspension

Texturing
---------
Texturing is possible in-world, yet has its rules in order to maintain a certain level of decency.

Erotically oriented pictures may only be used in appropriate/private spaces i.e. personal interiors or strip clubs. Same applies for any socially unacceptable pictures, they are to be kept out of interiors accessible for the public. 

Billboard texturing can only be done for official legal factions to promote themselves. Exterior re-texturing can be placed to advertise a business if permission is given by the `Development Team`_.

Third Party Programs
--------------------
All rules of Woodland County, most notably ones regarding advertisements, spamming, harassment, and trolling, etc, apply to all services that are officially part of and run by Woodland County.

These programs are not be used in character or for any sort of in character communication, unless you are in specific and monitored channels that have been approved by the Upper Administration Team such as "tactical" channels for law enforcement factions.

Audible Gestures
----------------
Audible gestures are obnoxious and OOC, and do not being in roleplay. Please keep this in mind when engaging in roleplay or near others with roleplay on-going.


:Potential Punishment: 24 hour suspension

Recording/Monitoring Devices
----------------------------
Monitoring devices are strictly prohibited, this is described as any object which has the ability to save conversations with or without a player's consent. These may be roleplayed, but are not to be actual scripts which save conversations.

:Potential Punishment: 30 day suspension

Administration Housekeeping
===========================
The following are to aid in administration and tasks, you agree to the following.

Consent to Save & Share Conversations
-------------------------------------
You give implied consent to Priority Zero Gaming Staff to share your conversations & information about you for adminsitration purposes and within the administration team. We may save conversations to aid in administrative tasks and provide as evidence in reports. Priority Zero Gaming Staff are not permitted to share this information outside of the Priority Zero Gaming Staff unless handling an administration issue.

Muting/Blocking Staff
---------------------
Muting or blocking staff on any of the Priority Zero Gaming Network or Woodland County is prohibited. If you have a complaint about how a staff member has handled something, please file a complaint through the `Support Portal`_. If you have muted/blocked a member of staff, your access may be restricted or limited within Priority Zero Gaming until the mute/block has been lifted.

:Potential Punishment: Suspension or removal from the community

Indemnification
===============
Priority Zero Gaming & Woodland County, nor it's owners, administrators, and staff (collectively "Priority Zero Gaming") is neither liable for damages due to the actions of its visitors, residents, and employees, nor is Woodland County liable for region performance problems, and/or the loss of no-copy items due to a rollback, sim crash, returned objects, or any other reason out of Woodland County’s control, etc. If you have lost inventory, you should contact Linden Lab directly.

Not Here To Roleplay
====================
The `Administration Team`_ have permission to issue a permanent suspension to anyone who is perceived to constantly disrupt the community to keep order within the community.

This can be determined as, but not limited to:

- Entering the community with obnoxious avatars
- Spamming anything to disrupt (sounds, objects, gestures, voice, etc.)
- Entering player's personal homes without approval
- Harassing members of the community

This type of suspension can be appealed via the `Support Portal`_ provided you tell the truth and if necessary, provide evidence to aid your report.

:Potential Punishment: Permanent suspension or removal from the community

No OOC Drama
============
OOC drama has no place here. Issues you have with other players outside of Priority Zero Gaming should be delt with outside and not brought into the community.

This includes passing notecards of chat logs and spreading OOC rumors either in or outside of Woodland County.

No Sharing Chatlogs
-------------------
Passing chat log notecards or other people's conversations without permission, except as agreed upon and provided herein.

If another player is harassing you, or in breach of any other rule, report them via the `Support Portal`_.

:Potential Punishment: Permanent Suspension or removal from the community

Staff Exemption
^^^^^^^^^^^^^^^
Priority Zero Gaming Staff are exempt from the above rule when operating in their duties. Including but not limited to, dealing with current issues, player reports, etc.

This is inline with `Consent to Save & Share Conversations`_ and cannot be used for any other purposes other than administration within Priority Zero Gaming.

This information can aid in processing requests and reports from players, but an administrator cannot force you to divulge chat logs.

Rule Updates
============
The Priority Zero Gaming Team reserve the right to modify these rules, regulations and general laws at any timewithout notice. It is up to you to ensure you check this document regularly.