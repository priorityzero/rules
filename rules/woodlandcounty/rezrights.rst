###########################
Rez Rights, Objects & Rules
###########################
.. note::

  These are the object rezzing rules which are in place to maintain order and stability in the server.

.. _Upper Administration Team: https://forums.priorityzerogaming.com/support/
.. _Administration Team: https://forums.priorityzerogaming.com/support/
.. _Development Team: https://forums.priorityzerogaming.com/support/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _idea tracker: https://forums.priorityzerogaming.com/ideas/
.. _bug tracker: https://forums.priorityzerogaming.com/bugs/

Tourists
========
Tourists are not permitted to rez.

They are permitted to walk around the sim to view the areas, however must keep respect of roleplays, other tourists and visitors.

Tourists that rez items on the sim will be approached and warned by Administration. Continuing to rez items onto the sim after warning may result in a kick, or ban, from the sim.

Citizens
========
Citizens who are not renting are permitted to rez one (1) vehicle for use while on the sim. This vehicle is expected to be removed once the citizen is leaving the sim unless part of on-going roleplay.

Citizens are permitted to rez items that pertain to an in-character roleplay they are a part of.

:Potential Punishment: Objects returned, removal of citizenship.

Residents (Renters)
===================
Residents and Renters are permitted to rez a vehicle for use while on the sim. This vehicle is expected to be removed, parked in approved parking spaces, or parked on the Resident's home once the citizen is leaving the sim.

Residents and renters are permitted to rez items that pertain to an in-character roleplay they are a part of. These items are expected to be picked up once the role-play is complete. 

Renters are permitted to rez items within their shop as decoration, or if they are items that the renter is selling. Land Impact must not exceed that of the specified amount on the rental box. These items can stay down “permanently”, as long as the renter continues to pay the required tier.

Residents are permitted to rez items within their home as they see fit. Residents can have multiple vehicles, as long as they remain parked up on the Resident’s parcel while they are not on sim. Vehicles will count against a Resident’s Land Impact. These items, within the Resident’s parcel, can stay down “permanently”, as long as the resident continues to pay the required tier.

Old items which may be left out by the previous tenant, will be returned as notified or discovered. Please note that some of our rentals are not fully furnished.

Prim Checks
===========
Administration will conduct "prim checks" at least once a week or as necessary.

Any further offense may lead to further tier needing to be paid, removal of prims by Administration or eviction from the house/shop being rented.

Overlimits
----------
Residents and Renters found exceeding their paid Land Impact will be warned for a first offense. 

Cleanup After Yourself
======================
Items are expected to be picked up once the role-play is complete. This can be done ICly by means of coroner, towtruck, etc.

:Potential Punishment: Objects returned, removal of citizenship.

On-going or long-term roleplays
-------------------------------
An exception to cleanup after yourself is roleplays which are considered on-going or long-term, this includes but not limited to:

- Police investigations
- Fire investigations
- Serial killers
- Recent fires
- Parking fines

It would be expected that these items are clean up as soon as they have been finished their purpose.

Vehicle Returns (Tow Trucks)
----------------------------
Law Enforcement are authorised to call in (NPC) Tow Trucks to return your items. This will be done by an Administrator who will notify you that your vehicle has been towed at the request of law enforcement. For further information, you should contact the law enforcement agency who requested the tow.

Towing Delay
^^^^^^^^^^^^
Vehicles which are being NPC towed must have a 5 minute delay. This delay can be requested to be increased to a maximum of 30 minutes by the requester. A countdown timer must be visible beside the vehicle being towed by the Administrator returning the vehicle. An optional tow truck may be placed next to the vehicle.

Exemption
"""""""""
Vehicles being returned for administrative purposes or prim cleanups are exempt from this rule.