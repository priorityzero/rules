##############
Roleplay Rules
##############
.. note::

  These are our core rules which are applied universally while roleplaying.

.. _Upper Administration Team: https://forums.priorityzerogaming.com/support/
.. _Administration Team: https://forums.priorityzerogaming.com/support/
.. _Development Team: https://forums.priorityzerogaming.com/support/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _idea tracker: https://forums.priorityzerogaming.com/ideas/
.. _bug tracker: https://forums.priorityzerogaming.com/bugs/

Always Roleplay
===============
All players must remain in character at all times unless allowed to go out of character by an administrator or they are themselves an administrator.

*"Roleplay now,* `report <https://forums.priorityzerogaming.com/support/>`_ *later."*

:Potential Punishment: 3 Day Suspension

Brawls
------
Standard procedure is to fight in an RP manner, however, players can OOCly agree to perform a fight in a brawl done with scripts and systems.

Vehicular Pursuits
------------------
An exception of always roleplay is doing a PIT maneuver during a chase.

Extreme or Disgusting Roleplay
------------------------------
An exception of Always Roleplay is listed in the Extreme or Disgusating Roleplay section.

Quality of Roleplay
===================
Quality of Roleplay is defined as failure to uphold the standards of roleplay expected by Priority Zero Gaming Community and Staff.

Poor quality roleplay will be looked at negatively and you may face punishment for the same. This includes having No Value of Life.

:Potential Punishment: 3 Day Suspension

Value of Life
-------------
The value of life is an economic value used to quantify the benefit of avoiding death. Failing to show a value of life, results in poor
quality roleplay.

Metagaming
==========
Metagaming is prohibitied. Metagaming is when someone uses out of character (OOC) information for in character (IC) purposes.

Attempting to incite metagaming is also prohibited.

:Potential Punishment: 7 Day Suspension

Powergaming
===========
Power gaming is prohibitied. Powergaming can be described as a player declaring their own action against another player as successful, without 
giving the other player the freedom to act on their own decisions. This includes but not limited to:

* Forcing actions upon a player.
* Failing to allow a player to roleplay their own actions.
* Use of items which you haven't physically obtained.
* Acting superhuman.

:Potential Punishment: 3 Day Suspension

Character Development
-----------------------
Character development plays a vital role in what is considered Powergaming. A character that goes to the gym and works out regularly
may be in better physical condition than others involved in the situation.

Special Characters
^^^^^^^^^^^^^^^^^^
Special Characters are characters which have a particular subset of skills (superior strength, shooting, stamina, etc), such as but not
limited to:

* Mentally Challenged
* Skilled Martial Artists
* Members of Special Operations Forces

These characters must have `Upper Administration Team`_ approval prior to roleplaying as such.

Vehicles
----------
Vehicles which are used as they are not designed, such a lowrider offroading is considered Powergaming as the vehicle would not be
able to sustain such conditions without breaking or becoming inoperable.

Vehicles have additional rules which can be found in the Vehicles section.

Deathmatching
=============
Deathmatching is prohibitied. Deathmatching is the act of killing another persons character without sufficient reason or proper roleplay.

:Potential Punishment: 7 Day Suspension

Death
============

Player Kills
------------
A player kill is when your character is killed, simulating unconsciousness and amnesia which extends as far back as that particular roleplay situation's beginning. If you are revived by an administrator to roleplay your wounds after being player killed, this does not reverse the amnesia effects unless an administrator specifically tells you the amnesia is voided for a particular reason. Player kills do not necessairly require a script death for the effects of a player kill to exist such as amnesia and severe injuries.

No application is needed to player kill someone. Only a solid in character reason.

Roleplaying After a Player Kill
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If a character is player killed and the scene is left after they respawn, the players involved should roleplay that the unconscious character is found by a good samaritan, emergency services were called, and they were taken to the hospital. The person that was player killed would then after they respawn at the hospital, roleplay in the hospital for a period of time for their injuries to heal and continue to roleplay those injuries accordingly.

Do not respawn and immediately run around as if nothing happened!

Character Kills
---------------
A character kill is when your character is permanently killed and the ability to access that character is forbidden. They serve as a means of permanently ending a character's life and their story. Character kills should not be taken lightly. Remember, a majority of situations can be dealt with before resorting to a character kill by beating, player kill, etc.

An `application <https://forums.priorityzerogaming.com/support/>`_ is necessary to character kill someone due to the severity of it. If your character kill application is sensitive, you may mark the request as Confidential. Private character kills may be sent to upper administration members as well for the most privacy. Additionally, character kills may be accepted in game by a single administrator for situations where you require a quick response where you otherwise could not wait for an application.

In order for a character kill to be valid, the character being killed should be killed in roleplay. There are a few types of exceptions to this. If for example someone's foot is exposed and it is shot over and over and that would be a player kill, they would not necessarily realistically die. Thus, if they received reasonable medical treatment before they bled out, they'd survive and lose their foot. On the other hand, if that person hadn't received prompt medical attention, they would bleed out and die, warranting a character kill even if they hadn't been player killed. Another example of this would be if someone is run over by a vehicle. With the game physics, they may not lose very much health, but in reality, they would very easily be killed at a high speed impact.

Character Kill Scenarios
^^^^^^^^^^^^^^^^^^^^^^^^
A character kill scenario is exactly as it sounds, a scenario of serious roleplay where character kills are issued to characters that die. These are common in roleplay events and large or serious situations.

In order for a character kill scenario to be valid, there must be proof. An administrator must witness the situation or there must be roleplay logs, a video, credible eye witnesses or comprehensive screen shots then shown to the handling administrator. Character kill scenarios may happen at any time an administrator deems necessary, thus, players should always be realistic and careful as they would in real life.

A character kill scenario may not necessarily be publicly mentioned to people involved to prevent metagaming where people shoot just to get a character kill where they otherwise wouldn't have shot if they didn't know them shooting someone was a guaranteed character kill on another player.

Character Kill Clauses
^^^^^^^^^^^^^^^^^^^^^^
Factions may have a character kill clause that you inherently take upon yourself by associating with them. These factions are generally illegal ones. In order for a character kill clause to be valid it must be present on their thread and submitted to the Faction Team so they are aware of it and can validate your claim of the clause in the future to ensure it is not being made up.

Generally character kill clauses for factions cover anyone who is an associate and above. The criteria for someone to be character killed is nearly endless and is generally approved by a leader of the faction. This is the inherent risk in being part of illegal roleplay. You are considered an “associate” and above if you willingly take part in illegal activity with an associate or member of the faction.

Emergencies, Incidents & Disasters
==================================
Emergencies, incidents and/or disasters may only be setup to a scale where the resources are available. 

Roleplays may be void by an Administrator if there is insufficient resources.

All fire incidents must have an initial cause & police related incidents should have some evidence left, as per the `Evidence`_ rule.

All Fires
---------
All fires emergency service employees are available to respond to the fire. Fires otherwise may be rezzed in public areas, public buildings, unowned rentals, 

Small Fires
^^^^^^^^^^^
Vehicles, small buildings without anyone inside, sheds, and similar places may be targeted with arson without firefighters available assuming the fire remains contained.

Large Fires
^^^^^^^^^^^
Large buildings, buildings with people inside, forest fires, and similar places may be targeted provided there are at least two firefighters available, or three or more if rescue is involved.

Rentals
^^^^^^^
Fires that will affect a rental require permission of the rental tenant or subtenant, the owner must also be online at the time of the fire.

It is up to the decision of the rental owner if they wish to roleplay any damages that occurred.

Permission is not needed if the rental is locked, reserved or unowned.

Government Buildings
^^^^^^^^^^^^^^^^^^^^
Permission require the permission of anyone within the department hierarchy. It is assumed that all gas stations & government buildings have fire detection systems.

It is up to the decision of the department head or building maintainer if they wish to roleplay any damages that occurred.

Exemption
^^^^^^^^^
Administrators and official scripted fires are exempt from the above, and do not need permission.

Extreme or Disgusting Roleplay
===============================
Being an 18+ environment, certain themes and scenes may come up to fit in with story lines and characters. However, the following are to require consent and forcing of any of the below will result in punishment.

:Potential Punishment: 3 Day Suspension

Consent
-------
Every party involved, including witnesses, must OOCly agree to participate in any of the situations listed below:

* Rape
* Cannibalism
* Bestiality
* Necrophilia
* Toreture
* Erotic Roleplay
* Sexual Harassment

You may withdraw your consent at anytime during the roleplay.

Prohibited
----------
Roleplay in the following list is prohibited in any circumstance:

* Sexual roleplay of minors (younger than 18)

Roleplay Binds
==============
Binds to draw or holster one handed weapons are allowed as they naturally have a faster draw time. Two handed weapons such as assault rifles, rifles, shotguns, etc. require a manually typed out /me to draw the weapon, unless it is easily accessible due to predetermined RP (gun racks, gun slings, gun on lap, etc).

Logging to Avoid
================
Players are forbidden from logging out during a roleplay unless approved by an Administrator. Do not join in a large roleplay situation if you cannot commit the time.

Law Enforcement Situations
--------------------------
After criminal activity in which Law Enforcement may become involved, you must wait 30 minutes prior to logging off.

Provoking
==========
Seeking attention from law enforcement or emergency services by shouting at them, making 911 calls to be chased, etc, is prohibited.

Evidence
=========
All actions may leave traces left behind from the roleplay. Such as, but not limited to:

* CCTV Footage
* Finger Prints
* Tire Treads or Shoe Imprints
* Broken Locks / Doors
* Glass Fragments
* Civilian Witnesses
* Etc.

Notes should be dropped indicating this evidence and information must be given to any overseeing administrators so they may relay the information to investigative parties.

CCTV Cameras
============
CCTV Cameras are by default, roleplayed as a 90 degree angle camera with 480p resolution at 5 frames per second. The data must be stored somewhere when roleplaying the install. All CCTV camera installations/upgrades must be approved by an administrator and added to a CCTV Database. Footage is wiped at the end of the week (Sunday) if nothing of significance has occurred unless otherwise specified.

All government buildings and gas stations are assumed to have sufficient cameras to cover most common angles both inside and outside.

Sharing Info Between Characters
===============================
Sharing information between your own characters is strictly prohibited. Regardless of if your characters share different or same gangs, any information learned on one character cannot be shared via your other characters. 

:Potential Punishment: 7 Day Suspension

LEO Presence
=============
Always act like there is a LEO presence, even if there is lack thereof. However if there is a noticeable lack of LEO presence, then try to play accordingly.

Abusing a lack of LEO presence is prohibited. 

:Potential Punishment: 3 Day Suspension

Issues In Character
===================
If any sort of issue arises in game - including but not limited to; rule breaks, drama, anything else you feel is an issue - you must finish the active RP scenario and then follow it up after the fact via the correct methods (forums, contact admin, etc) ASAP. 


Roleplay Zone
=============
All roleplay must be done within a confined zone known as "Woodland County, Vermont".

The list of approved zones for roleplay outside of Woodland County are:

#. None.

Rentals
-------
Rentals have different roleplay arrangements, and requirements. All players are subject to follow these rules as listed in the Rental Rules.

.. note::
  
  Please see Rental Rules for further classification and IC/OOC distinctions.

Laws
----
Woodland County is based in Vermont and as such, all in-character actions are subject to Vermont Laws.

Exemption
^^^^^^^^^
Any of the rules listed on this site will override Vermont Laws.

/me Command
===========
The /me command must be used for roleplay purposes only. This command can be used to express anything that cannot otherwise be said through words;

Example: Giving evidence that is on scene that should be able to be seen, showing facial expressions, actions that don’t have a gesture, etc.

On the other hand, /me is not to be used to display anything that would not be able to be physically seen;

Example: /me feels sad - This should be shown as /me looks sad.

Voice Chat
==========
Woodland County is a text-based roleplay environment, as such voice chat is considered OOC by default and should be treated as such.

Exemption
---------
If all parties involved in the roleplay agree to use voice chat, then this may be done.  If a new person comes into the scene, then approval for voice chat must be provided by them, otherwise the roleplay will fall back to text chat.

Being OOC
=========
Being out of character (OOC) should be kept to a minimum. All conversations must be wrapped with brackets.

**Breaking character is defined as you being in-character and then acting out of character is strictly forbidden and should never occur.** An example of this would be mixing IC & OOC chat in local conversation.

Local OOC
---------
If you are in local chat, all OOC messages must be wrapped with brackets. You should avoid words OOC words such as TP (Teleport), Login, Logoff, etc. in all IC chats. 

OMG SOMETHING STUPID/WIERD/LAG HAPPENED
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Second Life can be a buggy engine at the best and worst of times. If this happens, please try to treat the issue as in-character as possible. See some examples:

- OOC: You're driving and "lag" into a pole/lake/car/etc - IC: Your brakes failed, steering locked, etc.
- OOC: You're responding to a scene and your vehicle fails mid-sim-crossing - IC: The vehicle has a mechanical failure. 
- OOC: You have to leave a scene because real life - IC: You have appointments/errands to run and have to go.
- OOC: Something else weird happens - IC: Wizards. (Yes, we're serious, you can use this as an excuse).

Voiding A Scene
"""""""""""""""
Only the Administration Team can void a roleplay, if you wish to void a scene which has occured please contact an Online Administrator.

If there is no Online Administrator, please submit a request to the `Administration Team`_ detailing the events, provide evidence and a reason why you believe the roleplay should be voided.

Group Chats
-----------
All group chats are considered OOC and do not need to be wrapped in brackets.

Group Notices
^^^^^^^^^^^^^
Group notices which are not tagged OOC are exempt from the above and should be treated as in character.

Instant Messages
----------------
All instant messages are classified as OOC unless started with a /me and the party is aware they are being involved in a roleplay.