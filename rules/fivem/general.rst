#############
General Rules
#############
.. note::

  The Los Santos Roleplay General Rules are all the rules that are related to players, accounts and not focused on in character actions.

.. _Upper Administration Team: https://forums.priorityzerogaming.com/support/
.. _Administration Team: https://forums.priorityzerogaming.com/support/
.. _idea tracker: https://forums.priorityzerogaming.com/ideas/
.. _bug tracker: https://forums.priorityzerogaming.com/bugs/

18+ Only
=============================
All players must be 18+ only. We are an adult community, there are adult themes and adult language being used. Minor characters are moderated as per rules.

:Potential Punishment: Removal from the community

Toxicity and Bullying
========================
Toxicity is defined as bringing unwanted attitudes into the community.

There is a difference between having an opinion/voicing this opinion, and forcing your opinion on other people.

Do not direct your anger/frustration/aggression towards another player or staff. Use the appropriate forums for this.

:Potential Punishment: 7 Day Suspension

Derogatory Remarks (IC & OOC)
=============================================
Negative/Derogatory remarks about; race, religion, gender, sexual preference, sexual orientation, gender identity, etc. is strictly prohibited. This includes in and out of RP.

:Potential Punishment: 7 Day Suspension

Do not contacting developers directly
=====================================
Please use the relevant forums to get help with your issues. Before attempting to message developers. Only message with permission if absolutely necessary.

:Potential Punishment: 24 Hour Suspension

Scamming
======================
Scamming of money, vehicles and properties is not allowed by any means both IC or OOC. In general this means performing an action which gives you legal possession of a property or vehicle through deceit.  

**Example of Scamming**
Promising to pay money after someone sells you a property or vechicle and not paying the money as promised.

**Example of Not Scamming**
Taking a property or vehicle through force or robbery, in this instance, possession was never taken legally through deceit.

:Potential Punishment: 7 Day Suspension

AFKing
=============================
Leaving your computer or in any manner tabbing out or going AFK should not be done in a public area, please be inside your private home or the welcome center.

:Potential Punishment: 7 Day Suspension

External Advertising
=========================
Sending other players links or discussing other servers, communities or groups in the manner of advertising is strictly prohibited. We expect all players to keep their opinions about said matter to themselves, as we hope members from other corners of the gaming world would do the same for us. 

:Potential Punishment: 7 Day Suspension

Report Bugs
===========
You are not allowed to abuse bugs, bugs are considered as actions in which are not intended, such as duplication of items/money/vehicles.
You are required to report any bugs you find using our `bug tracker`_. 
Failure to report bugs and instead using them for your own benefit/gain will be met with a severe punishment (see below).

Exploiting
========================
Exploiting of a bug or glitch is strictly prohibited. This includes setting up macros to farm jobs. Purposely exploiting, duplicating or macro’ing anything for personal gain will result in heavy consequences.

Abusing game mechanics in unintended ways for clear non-roleplay gain is strictly prohibited.

:Potential Punishment: 7 Day Suspension

Impersonation
===========================
Impersonating another player, by using a similar account name, creating a certain level of confusion is not allowed. Account names are considered personal and must remain so.

Portraying yourself as a staff member is strictly forbidden and may lead to harsh punishments. Los Santos Roleplay chooses its staff members carefully and wishes to keep doing so.

:Potential Punishment: 3 Day Suspension

Inappropriate & Illegal Content
=============================================
Do not post material or links to content which is illegal as per United States, Canadian, Australian and International law.

Do not post nudity, gore, or otherwise inappropriate content without spoilers, warnings, and a reason. For example, illegal roleplayers discussing cartels in real life and showing a video of a shootout would be acceptable, showing random gore for shock value is not.

:Potential Punishment: 3 Day Suspension

Third Party Programs
--------------------
All rules of Priority Zero Gaming apply to all services that are officially part of and run by Priority Zero Gaming.

Third Party Communication
^^^^^^^^^^^^^^^^^^^^^^^^^
These programs or channels are not be used in character or for any sort of in character communication, unless you are in specific and monitored channels that have been approved by the Upper Administration Team such as "tactical" channels for law enforcement factions.

This includes private Discords which are created for use with Priority Zero Gaming.

:Potential Punishment: 3 Day Suspension

External Websites
=================
Any domain or program not under direct control of Priority Zero Gaming or Los Santos Roleplay (not an Los Santos Roleplay website) may not be utilized for in character purposes. Examples of this would be an external website created to advertise an in character business, a Dropbox to share an in character PDF document or utilization of any Google service such as docs, spreadsheets, etc, mainly used for organization purposes.
 
Exceptions to this rule are approved by the Upper Administration Team on a case-by-case basis and listed publicly. Exceptions will be made under the pretense that the platform being approved allows for full access by the Upper Administration Team as requested in order to prevent meta gaming. You can file for approval via a UAT with valid reasoning.
 
**Exceptions:**
 
* Google Drive (Docs, spreadsheets, etc.)

  * Los Santos Police Department
  * Blane County Sheriff'sfice
  * San Andreas  Highway  
  * Los Santos Fire Department Of
  * Los Santos County Government
  * Superior Court of San Andreas

* Airtable (Spreadsheets, databases, etc.)

  * Woodland County Police Force
  * Woodland County Sheriff's Office
  * Woodland County State Police
  * Woodland County Fire & Rescue
  * Woodland County DMV
  * Woodland County Public Works

* Discord (VoIP Communication)

  * Priority Zero Gaming
  * Priority Zero Gaming - Los Santos Roleplay
  * The Reapers M.C.

Twitch Terms of Service
=======================
We have a lot of players who stream their experiences on "Twitch", as such, with respect to them you are required to follow the Twitch Terms of Service which can be found here: https://www.twitch.tv/p/legal/terms-of-service/

Generally speaking, our rules try to line up with these ToS also.
