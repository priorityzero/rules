##############
Roleplay Rules
##############
.. note::

  These are our core rules which are applied universally while roleplaying.

.. _Upper Administration Team: https://forums.priorityzerogaming.com/support/
.. _Administration Team: https://forums.priorityzerogaming.com/support/
.. _Development Team: https://forums.priorityzerogaming.com/support/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _idea tracker: https://forums.priorityzerogaming.com/ideas/
.. _bug tracker: https://forums.priorityzerogaming.com/bugs/

Always Roleplay
===============
All players must remain in character at all times unless allowed to go out of character by an administrator or they are themselves an administrator performing administration duties.

If there is a problem, roleplay the scenario out and then report it.

*"Roleplay now,* `report <https://forums.priorityzerogaming.com/support/>`_ *later."*

:Potential Punishment: 3 Day Suspension

Extreme or Disgusting Roleplay
------------------------------
An exception of Always Roleplay is listed in the Extreme or Disgusating Roleplay section.

Breaking Character
------------------
Breaking character is severely prohibited and never allowed.

**Breaking character is defined as you being in-character and then acting out of character is strictly forbidden and should never occur.** An example of this would be mixing IC & OOC chat in local conversation.

:Potential Punishment: 24 Hour Suspension

OMG SOMETHING STUPID/WIERD/LAG HAPPENED
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
FiveM can be a buggy at the best and worst of times. If this happens, please try to treat the issue as in-character as possible. See some examples:

- OOC: You have to leave a scene because real life - IC: You have appointments/errands to run and have to go.
- OOC: Something else weird happens - IC: "It's probably aliens!"

Voiding A Scene
"""""""""""""""
Only the Administration Team can void a roleplay, if you wish to void a scene which has occured please contact an Online Administrator.

If there is no Online Administrator, please submit a request to the `Administration Team`_ detailing the events, provide evidence and a reason why you believe the roleplay should be voided.

Quality of Roleplay
===================
Quality of Roleplay is defined as failure to uphold the standards of roleplay expected by Priority Zero Gaming Community and Staff.

Poor quality roleplay will be looked at negatively and you may face punishment for the same. This includes having No Value of Life.

:Potential Punishment: 3 Day Suspension

Value of Life
-------------
The value of life is an economic value used to quantify the benefit of avoiding death. Failing to show a value of life, results in poor
quality roleplay.

Character Guidelines
--------------------
When creating a character, you are beginning a story for someone, not just putting a name to a model.

Consider the following:

* What are the personality traits of your character? Shy or confident? Hard working or lazy?
* What is your character's background? Where do they come from? While this doesn't have to be thorough, it will make it easier to roleplay the character.
* Give your character some strengths and weaknesses.
* Think about how your character will react to certain situations.
* Stick to your character's traits, you are playing the life of this character, so their experiences should have an impact on them. Let the consequences of your actions have an effect on your character.
* Don't get involved in everything. Don't try to be everything. If there's something else you're interested in, make another character and build a story up to it.
* Be mindful about how to enter on-going situations and other's story lines. You can interact with people and see if an opportunity shows, but don't try to force ot steer a current storyline if your story hasn't let up to it.
* Once your character has reached or failed to reach their goals, it may be time to consider bringing the character's story to an end. Perhaps consider other new characters you might want to play.


Metagaming
==========
Metagaming is prohibitied. Metagaming is purposely using or relaying information which your character did not learn in themself. It can also be defined as using out of character (OOC) information for in character (IC) purposes.

Attempting to incite metagaming is also prohibited.

:Potential Punishment: 7 Day Suspension

Powergaming
===========
Powergaming is prohibitied. Powergaming can be described as a roleplaying outside your character's abilities and traits, abusing mechanics and using means not actually in game to gain an advantage, such as:

* Springing/fighting as soon as you leave the hospital or are revived after being downed.
* Talking while you are unconscious.
* Acting or having unrealistic, superhuman, superpowers.
* Not using /me to notify others of something they should notice. Example: if you were shot or stabbed, and someone /me looks over yourbody - you should reply with /me has stab/shot wounds.
* Not realistically fearing for your life when you are threatened with weapons or severe harm from other means.
* Use of items which you haven't physically obtained.
* Jumping around or "bunnyhopping" to move around faster.
* Not keeping to your physical character's form. Example: If you are old or overweight, you should always have a "physical level" in mind and roleplay to this.

:Potential Punishment: 3 Day Suspension

Character Development
-----------------------
Character development plays a vital role in what is considered Powergaming. A character that goes to the gym and works out regularly
may be in better physical condition than others involved in the situation.

Special Characters
^^^^^^^^^^^^^^^^^^
Special Characters are characters which have a particular subset of skills (superior strength, shooting, stamina, etc), such as but not
limited to:

* Mentally Challenged
* Skilled Martial Artists
* Members of Special Operations Forces

These characters must have `Upper Administration Team`_ approval prior to roleplaying as such.

Vehicles
--------
Vehicles which are used as they are not designed, such a lowrider offroading is considered Powergaming as the vehicle would not be
able to sustain such conditions without breaking or becoming inoperable.

Character Guidelines
=

Deathmatching
=============
Deathmatching is prohibitied. Deathmatching is the act of killing another persons character without sufficient reason, proper roleplay and a story leading up to it.

:Potential Punishment: 7 Day Suspension


Death
============

Player Kills
------------
A player kill is when your character is killed, simulating unconsciousness and amnesia which extends as far back as that particular roleplay situation's beginning. If you are revived by an administrator to roleplay your wounds after being player killed, this does not reverse the amnesia effects unless an administrator specifically tells you the amnesia is voided for a particular reason. Player kills do not necessairly require a script death for the effects of a player kill to exist such as amnesia and severe injuries.

No application is needed to player kill someone. Only a solid in character reason.

Roleplaying After a Player Kill / New Life Rule
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If a character is player killed and the scene is left after they respawn, the players involved should roleplay that the unconscious character is found by a good samaritan, emergency services were called, and they were taken to the hospital.

The person that was player killed would then after they respawn at the hospital, roleplay in the hospital for a period of time for their injuries to heal and continue to roleplay those injuries accordingly.

Do not respawn and immediately run around as if nothing happened!

Character Kills/Permadeath
--------------------------
A character kill is when your character is permanently killed and the ability to access that character is forbidden.

They serve as a means of permanently ending a character's life. This can have a massive impact not only on their story, but the stories of characters connected to it.

In order for a character kill to be valid, the character being killed should be killed in roleplay, validated by a responding emergency service.

If you wish to character kill your character, you and have them dead for good, all you need to do is give the responding person the right indicators.

Note: You can not bring back a character killed character.

Character Kill Scenarios
^^^^^^^^^^^^^^^^^^^^^^^^
A character kill scenario is exactly as it sounds, a scenario of serious roleplay where character kills are issued to characters that die. These are common in roleplay events and large or serious situations.

In order for a character kill scenario to be valid, there must be proof. An administrator must witness the situation or there must be roleplay logs, a video, credible eye witnesses or comprehensive screen shots then shown to the handling administrator.

Character kill scenarios may happen at any time the Upper Administration Team deems necessary, thus, players should always be realistic and careful as they would in real life.

A character kill scenario may not necessarily be publicly mentioned to people involved to prevent metagaming where people shoot just to get a character kill where they otherwise wouldn't have shot if they didn't know them shooting someone was a guaranteed character kill on another player.

Character Kill Clauses
^^^^^^^^^^^^^^^^^^^^^^
Factions may have a character kill clause that you inherently take upon yourself by associating with them. These factions are generally illegal ones. In order for a character kill clause to be valid it must be present on their thread and submitted to the Faction Team so they are aware of it and can validate your claim of the clause in the future to ensure it is not being made up.

Generally character kill clauses for factions cover anyone who is an associate and above. The criteria for someone to be character killed is nearly endless and is generally approved by a leader of the faction. This is the inherent risk in being part of illegal roleplay. You are considered an “associate” and above if you willingly take part in illegal activity with an associate or member of the faction.

Extreme or Disgusting Roleplay
===============================
Being an 18+ environment, certain themes and scenes may come up to fit in with story lines and characters. The following require consent or are off limits entirely, and forcing of any of the below will result in punishment.

:Potential Punishment: 3 Day Suspension

Consent Required
----------------
Every party involved, including witnesses, must OOCly agree to participate in any of the situations listed below:

* Torture
* Cannibalism

Off-Limits
----------
However, the following strictly off limits and will result in punishment.

* Rape
* Beastiality
* Erotic or Sexual Roleplay
* Sexual Harassment

This is also to ensure we are inline with streaming provider's Terms of Service.


Extreme or Disgusting Roleplay
===============================

:Potential Punishment: 3 Day Suspension

Consent
-------
Every party involved, including witnesses, must OOCly agree to participate in any of the situations listed below:

* Rape
* Cannibalism
* Beastiality
* Necrophilia
* Torture
* Erotic Roleplay
* Sexual Harassment

You may withdraw your consent at anytime during the roleplay.

Prohibited
----------
Roleplay in the following list is prohibited in any circumstance:

* Sexual roleplay of minors (younger than 18)

Interaction Guidelines
======================
From time to time, you will interact with different characters from all walks of life, including government officials, small business owners, criminals, etc. The goal is for each player to provide each other with an immersive experience and add to each other's stories.

:Potential Punishment: 3 Day Suspension

EMS Interactions
----------------
IF you are downed (either by accident or a victim of a crime) you must wait for police or EMS to arrive.

When help arrives both sides should strive to provide a good scenario for each other.

EMS will act in a professional manner during the situation and the patient will roleplay out their injuries to the best of their ability.

Use of /me to provide extra information of vitals and injuries is encouraged.

LEO Interactions
----------------
Police will behave in a professional manner towards the public and each other while the public will provide a base level of respect you'd generally give to police.

Use of /me to provide extra information for investigations is encouraged.

Arrest/Investigation Scenarios
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
When a player is downed or put into handcuffs by police, they are considered in being in police custody.

Once in custody, the current crime or putsuit scenario is over and switches to an arrest/investigation scenario.

During these scenarios, those involved with the person(s)s under arrest may no longer interfere and civilians not needed for questioning or involved should leave the area.

Recording and Photos
--------------------
Players may take pictures with their phone using the emote. They may also record using a camera. The camera may pick up the audio if the player is in range to hear the audio (not through walls/glass, etc).

It is expected that most police will have BWC (Body Worn Cameras) and ICV (In Car Video) of incidents which can be controlled by the officer.

Certain areas of the map and buildings may have CCTV which can be accessed.

Screenshots, Videos & Streams may be supplied to aid in these cases but are not required.

Criminal Guidelines
===================
Here are some things to consider when making a criminal character.

Build Up and Pacing
-------------------
Don't just rush in to be a criminal mastermind on day one. Let your character make connections and learn how crime works in the city before making big moves like robbing banks or taking on potential rivals.

Heat and Law Enforcement
------------------------
Criminals should be mindful that their actions may generate attention from police. Give yourself enough time between your crimes for things to cool down.

You should expect to get more police attention, longer jail times/fines, and investigations if you continue to repeatly commit crimes in a small time frame.

Criminal Hierarchy
------------------
Eventually your character may learn or come in contact with the structure of the criminal world.

All criminals had to work their way up this structure and at some point this climb can be very important to their story.

Making connections in this world will help evolve your character's story.

Conflict
--------
While some organizations and criminals may work together, from time to time, conflict is expected, whether territory, resources, consumers, and power may drive many of these conflicts.

Conflict and cooperation can help make both stories interesting, be sure to have a good mix.

Avoid conflicts with large alliances which don't go well with conflict (including Law Enforcement).


Logging to Avoid
================
Players are forbidden from logging out during a roleplay unless approved by an Administrator. Do not join in a large roleplay situation if you cannot commit the time.

Criminal Activity
-----------------
After criminal activity in which Law Enforcement may become involved, you must wait 30 minutes prior to logging off.


Sharing Info and Items Between Characters
===============================
Sharing information, items or money between your own characters is strictly prohibited. Regardless of if your characters share different or same gangs, any information learned on one character cannot be shared via your other characters. 

You may not transfer information, items or money via a third-party or other player. If you have a need to do this, submit a request to the `Upper Administration Team <https://forums.priorityzerogaming.com/support/>`_.

:Potential Punishment: 7 Day Suspension


LEO Presence
=============
Always act like there is a LEO presence, even if there is lack thereof. However if there is a noticeable lack of LEO presence, then try to play accordingly.

Abusing a lack of LEO presence is prohibited. 

:Potential Punishment: 3 Day Suspension

Cop Baiting
-----------
Committing a crime with the sole intention of drawing police away from another crime regardless of size is prohibited.

Example: Shooting or distracting police so your friend can get out of a ticket, or shooting around the map before committing a crime so that police are tied up searching for a shooter when you've long left the area.

It is not prohibited to harass or commit offenses in front of police, however you should expect IC repercussions. 

If you are unsure whether or not your actions will be considered Cop Baiting, please contact the `Upper Administration Team <https://forums.priorityzerogaming.com/support/>`_.

:Potential Punishment: 1 Day Suspension

Issues In Character
===================
If any sort of issue arises in game - including but not limited to; rule breaks, drama, anything else you feel is an issue - you must finish the active RP scenario and then follow it up after the fact via the correct methods (forums, contact admin, etc) ASAP. 


Environment
===========
The setting for the story is in the State of San Andreas and the counties within it.

The state is made up of several million people with your characters being a sample of the people in it.

While your characters may vary depending on what you prefer, the State of San Andreas will act and should be treated as a realistic state. A lot of balance for a character may come from this.

Example: If a player devices to make a psycho killer and go on a spree, the player should expect this type of character to be short-lived and once caught, be punished to the fullest extend of the law.


/me Command
===========
The /me command must be used for roleplay purposes only. This command can be used to express anything that cannot otherwise be said through words;

Example: Giving evidence that is on scene that should be able to be seen, showing facial expressions, actions that don’t have a gesture, etc.

On the other hand, /me is not to be used to display anything that would not be able to be physically seen;

Example: /me feels sad - This should be shown as /me looks sad.

