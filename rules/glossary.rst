#######################
Glossary & Common Terms
#######################

.. note::

  This is a glossary of common terms and words which are used throughout all Priority Zero Gaming, some terms may be specific to a related game.
  
.. _Community: https://priorityzerogaming.com
.. _Forums: https://forums.priorityzerogaming.com/
.. _Support Portal: https://forums.priorityzerogaming.com/support/
.. _Ideas Portal: https://forums.priorityzerogaming.com/ideas/
.. _Bug Tracker: https://forums.priorityzerogaming.com/bugs/
.. _Rule Discussions: https://forums.priorityzerogaming.com/rulediscussions/

Administrator
  A staff member tasked with being the standard "problem-solver". They are taking the majority of reports, helping and guiding players and are the front-line of the community. In Second Life, this role is commonly known as an Estate Manager.

Avatar
  The appearance of the character controlled by the player.

Developer
  A staff member tasked with developing new gameplay mechanics, fixing bugs, and improving the general aesthetic of the game world. In Second Life, this is commonly known as a Sim Builder.

Furry
  An anthropomorphic character, typically represented as a bipedal animal with human traits.

Gesture
  A macro that may display text in local chat, play a sound, play an animation, or any combination of the three.

Land Impact
  How much an object subtracts from the allotment of prims allowed on a Second Life sim.

Prim
  An object spawned into the game world.

Region
  The area in which the game world is built. Used interchangably with "Sim".

Rez
  The act of spawning an object into the game world.

Roleplay
  Acting or performing the part of a person or character.

Sim
  The area in which the game world is built. Used interchangably with "Region".

SL
  Second Life

Soon
  Soon does not imply any particular date, time, decade, century, or millennia in the past, present, and certainly not the future. Soon shall make no contract or warranty between Priority Zero Gaming and the end user. Soon will arrive some day, Priority Zero Gaming does guarantee that soon will be here before the end of time. Maybe. Do not make plans based on soon as Priority Zero Gaming will not be liable for any misuse, use, or even casual glancing at soon.